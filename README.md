First, we run our 
$api.py
to get the server running

Info “Serving on …..” will show on the screen

We can now test api/books?attr={attr_value}

eg: http://0.0.0.0:8080/api/books?title=for

We get output in json format

We have api for get, put, post and delete

We support and, or operation by character "AND_" "OR_"

eg:http://0.0.0.0:8080/api/books?author=Robert%20C.%20MartinAND_for

We can view data in table format in rendering part

We can query most book authors