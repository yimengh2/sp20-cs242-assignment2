import flask
from flask import request, jsonify
from pymongo import MongoClient
from bson import json_util
from waitress import serve

from flask import render_template
from bokeh.plotting import figure
from bokeh.embed import components

app = flask.Flask(__name__)
app.config["DEBUG"] = True

client = MongoClient("mongodb+srv://yimengh2:AlinaHan910"
                          "@cluster0-oevn3.mongodb.net/test?retryWrites=true&w=majority")
book_collection = client.my_database.book_collection
author_collection = client.my_database.author_collection

#(1) titles, (2) authors, and (3) related books
@app.route('/api/books', methods=['GET'])
def api_get_book():
    '''api get book by attributes'''
    results = []
    #http://0.0.0.0:8080/api/books?title=for
    if 'title' in request.args:
        title = request.args['title']
        print(title)
        #handle and condition
        if 'AND_' in title:
            and_title = title.split('AND_')
            title_1 = and_title[0]
            title_2 = and_title[1]
            #http://0.0.0.0:8080/api/books?title=forAND_Clean
            cur = book_collection.find( {"$and":[{'title': {"$regex": ".*" + title_1 + ".*"}},
                                                {'title': {"$regex": ".*" + title_2 + ".*"}}] })
            #cur = book_collection.find({'title':{"$regex": ".*" + title+ ".*"}})
            for doc in cur:
                #print(doc)
                results.append(json_util.dumps(doc))
        #handle or condition
        elif 'OR_' in title:
            or_title = title.split('OR_')
            title_1 = or_title[0]
            title_2 = or_title[1]
            #http://0.0.0.0:8080/api/books?title=CoderOR_for
            cur = book_collection.find( {"$or":[{'title': {"$regex": ".*" + title_1 + ".*"}},
                                                {'title': {"$regex": ".*" + title_2 + ".*"}}] })
            for doc in cur:
                results.append(json_util.dumps(doc))
        #when no and, or condition to handle
        else:
            cur = book_collection.find({'title':{"$regex": ".*" + title+ ".*"}})
            for doc in cur:
                results.append(json_util.dumps(doc))

        response = jsonify(results)
        response.status_code = 200 # Provides a response status code of 200 which is "OK"
        print(response.status_code)
        return response, response.status_code

    #get book by author
    elif 'author' in request.args:
        author = request.args['author']
        print(author)
        #handle and condition
        if 'AND_' in author:
            and_author = author.split('AND_')
            author_1 = and_author[0]
            author_2 = and_author[1]
            #http://0.0.0.0:8080/api/books?author=Robert%20C.%20MartinAND_for
            cur = book_collection.find( {"$and":[{'author': {"$regex": ".*" + author_1 + ".*"}},
                                                {'author': {"$regex": ".*" + author_2 + ".*"}}] })
            for doc in cur:
                results.append(json_util.dumps(doc))
        #handle or condition
        elif 'OR_' in author:
            or_author = author.split('OR_')
            author_1 = or_author[0]
            author_2 = or_author[1]
            #http://0.0.0.0:8080/api/books?author=Robert%20C.%20MartinOR_for
            cur = book_collection.find( {"$or":[{'author': {"$regex": ".*" + author_1 + ".*"}},
                                                {'author': {"$regex": ".*" + author_2 + ".*"}}] })
            for doc in cur:
                results.append(json_util.dumps(doc))
        #when no condition to handle
        else:
            cur = book_collection.find({'author':{"$regex": ".*" + author+ ".*"}})
            for doc in cur:
                results.append(json_util.dumps(doc))
        response = jsonify(results)
        response.status_code = 200 # Provides a response status code of 200 which is "OK"
        print(response.status_code)
        return response, response.status_code

    elif 'related_book' in request.args:
        related_book = request.args['related_book']
        if 'AND_' in related_book:
            and_author = related_book.split('AND_')
            author_1 = and_author[0]
            author_2 = and_author[1]
            cur = book_collection.find( {"$and":[{'similar_books': {"$regex": ".*" + author_1 + ".*"}},
                                                {'similar_books': {"$regex": ".*" + author_2 + ".*"}}] })
            for doc in cur:
                results.append(json_util.dumps(doc))

        elif 'OR_' in related_book:
            or_author = related_book.split('OR_')
            author_1 = or_author[0]
            author_2 = or_author[1]
            cur = book_collection.find( {"$or":[{'similar_books': {"$regex": ".*" + author_1 + ".*"}},
                                                {'similar_books': {"$regex": ".*" + author_2 + ".*"}}] })
            for doc in cur:
                results.append(json_util.dumps(doc))
        else:
            cur = book_collection.find({'similar_books':{"$regex": ".*" + related_book + ".*"}})
            for doc in cur:
                results.append(json_util.dumps(doc))

        response = jsonify(results)
        response.status_code = 200 # Provides a response status code of 200 which is "OK"
        print(response.status_code)
        return response, response.status_code


    else:
        response = jsonify(results)
        response.status_code = 400 #bad data
        print(response.status_code)
        return "Error"
    #print(results)



#support searching for: (1) name, (2) book title
@app.route('/api/authors', methods=['GET'])
def api_get_authors():
    '''api get author by attributes'''
    #http://0.0.0.0:8080/api/authors?name=RobertOR_for
    results = []
    if 'name' in request.args:
        name = request.args['name']
        print(name)
        if 'AND_' in name:
            and_name = name.split('AND_')
            name_1 = and_name[0]
            name_2 = and_name[1]
            #http://0.0.0.0:8080/api/books?title=forAND_Clean
            cur = author_collection.find( {"$and":[{'name': {"$regex": ".*" + name_1 + ".*"}},
                                                {'name': {"$regex": ".*" + name_2 + ".*"}}] })
        elif 'OR_' in name:
            or_name = name.split('OR_')
            name_1 = or_name[0]
            name_2 = or_name[1]
            #http://0.0.0.0:8080/api/books?title=CoderOR_for
            cur = author_collection.find( {"$or":[{'name': {"$regex": ".*" + name_1 + ".*"}},
                                                {'name': {"$regex": ".*" + name_2 + ".*"}}] })
        else:
            print(name)
            cur = author_collection.find({'name':{"$regex": ".*" + name+ ".*"}})

        for doc in cur:
            results.append(json_util.dumps(doc))
        response = jsonify(results)
        response.status_code = 200 # Provides a response status code of 200 which is "OK"
        print(response.status_code)
        return response, response.status_code

    #find title of book in book_collection, get author name, and find author in author_collection
    elif 'book_title' in request.args:
        #http://0.0.0.0:8080/api/authors?book_title=Coder
        book_title = request.args['book_title']

        if 'AND_' in book_title:
            and_name = book_title.split('AND_')
            name_1 = and_name[0]
            name_2 = and_name[1]
            cur = book_collection.find({"$and": [{'title': {"$regex": ".*" + name_1 + ".*"}},
                                                   {'title': {"$regex": ".*" + name_2 + ".*"}}]})

        elif 'OR_' in book_title:
            and_name = book_title.split('OR_')
            name_1 = and_name[0]
            name_2 = and_name[1]
            cur = book_collection.find({"$or": [{'title': {"$regex": ".*" + name_1 + ".*"}},
                                                   {'title': {"$regex": ".*" + name_2 + ".*"}}]})
        else:
            cur = book_collection.find({'title': {"$regex": ".*" + book_title + ".*"}})

        for doc in cur:
            book_author = doc['author']
            author_info = author_collection.find({'name': book_author})
            results.append(json_util.dumps(author_info))

        response = jsonify(results)
        response.status_code = 200
        print(response.status_code)
        return response, response.status_code

    else:
        response = jsonify(results)
        response.status_code = 400 #bad data
        print(response.status_code)
        return "Error"




#http://0.0.0.0:8080/api/books?book_url=https://www.goodreads.com/book/show/3735293-clean-code
#{"title":"key123"}
@app.route('/api/books', methods=['PUT'])
def api_put_books():
    '''Put, or update the first entry of the item returned from the given attribute'''
    results = []
    input = request.json
    print(input)
    print(type(input))
    if 'book_url' in request.args:
        name = request.args['book_url']
        origin_document = book_collection.find_one_and_update({"book_url": name},{"$set": input})
    elif 'title' in request.args:
        name = request.args['title']
        origin_document = book_collection.find_one_and_update({"title": name},{"$set": input})
    elif 'book_id' in request.args:
        name = request.args['book_id']
        origin_document = book_collection.find_one_and_update({"book_id": name},{"$set": input})
    elif 'isbn' in request.args:
        name = request.args['isbn']
        origin_document = book_collection.find_one_and_update({"isbn": name},{"$set": input})
    elif 'author_url' in request.args:
        name = request.args['author_url']
        origin_document = book_collection.find_one_and_update({"author_url": name}, {"$set": input})
    elif 'author' in request.args:
        name = request.args['author']
        origin_document = book_collection.find_one_and_update({"author": name},{"$set": input})
    elif 'rating' in request.args:
        name = request.args['rating']
        origin_document = book_collection.find_one_and_update({"rating": name},{"$set": input})
    elif 'rating_count' in request.args:
        name = request.args['rating_count']
        origin_document = book_collection.find_one_and_update({"rating_count": name}, {"$set": input})
    elif 'review_count' in request.args:
        name = request.args['review_count']
        origin_document = book_collection.find_one_and_update({"review_count": name}, {"$set": input})
    elif 'image_url' in request.args:
        name = request.args['image_url']
        origin_document = book_collection.find_one_and_update({"image_url": name}, {"$set": input})
    elif 'similar_books' in request.args:
        name = request.args['similar_books']
        origin_document = book_collection.find_one_and_update({"similar_books": name}, {"$set": input})
    else:
        response = jsonify(results)
        response.status_code = 400  # bad data
        print(response.status_code)
        return "Error"

    results.append(json_util.dumps(origin_document))
    print(results)

    response = jsonify(results)
    response.status_code = 200
    print(response.status_code)
    #return origin file
    return response


#http://0.0.0.0:8080/api/authors?author_url=https://www.goodreads.com/author/show/7160453.Andy_Hunt
@app.route('/api/authors', methods=['PUT'])
def api_put_authors():
    '''Put, or update the first entry of the item returned from the given attribute in author_collection'''
    results = []
    input = request.json
    print(input)
    print(type(input))
    if 'name' in request.args:
        name = request.args['name']
        origin_document = book_collection.find_one_and_update({"name": name},{"$set": input})
    elif 'author_url' in request.args:
        name = request.args['author_url']
        origin_document = book_collection.find_one_and_update({"author_url": name},{"$set": input})
    elif 'author_id' in request.args:
        name = request.args['author_id']
        origin_document = book_collection.find_one_and_update({"author_id": name},{"$set": input})
    elif 'rating' in request.args:
        name = request.args['rating']
        origin_document = book_collection.find_one_and_update({"rating": name},{"$set": input})
    elif 'rating_count' in request.args:
        name = request.args['rating_count']
        origin_document = book_collection.find_one_and_update({"rating_count": name}, {"$set": input})
    elif 'review_count' in request.args:
        name = request.args['review_count']
        origin_document = book_collection.find_one_and_update({"review_count": name}, {"$set": input})
    elif 'image_url' in request.args:
        name = request.args['image_url']
        origin_document = book_collection.find_one_and_update({"image_url": name}, {"$set": input})
    elif 'related_authors' in request.args:
        name = request.args['related_authors']
        origin_document = book_collection.find_one_and_update({"related_authors": name}, {"$set": input})
    elif 'author_books' in request.args:
        name = request.args['author_books']
        origin_document = book_collection.find_one_and_update({"author_books": name}, {"$set": input})
    else:
        response = jsonify(results)
        response.status_code = 400  # bad data
        print(response.status_code)
        return "Error"

    results.append(json_util.dumps(origin_document))

    response = jsonify(results)
    response.status_code = 200
    print(response.status_code)
    #return origin file
    return response


@app.route('/api/book', methods=['POST'])
def api_post_book():
    '''Leverage POST requests to ADD A book to the backend (database)'''
    results = []
    input = request.json
    book_collection.insert_one(input)
    results.append({"insert_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response

@app.route('/api/books', methods=['POST'])
def api_post_books():
    '''Leverage POST requests to ADD SEVERAL books to the backend (database)'''
    results = []
    input = request.json
    book_collection.insert_many(input)
    results.append({"insert_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response


@app.route('/api/author', methods=['POST'])
def api_post_author():
    '''Leverage POST requests to ADD AN author to the backend (database)'''
    results = []
    input = request.json
    author_collection.insert_one(input)
    results.append({"insert_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response

@app.route('/api/authors', methods=['POST'])
def api_post_authors():
    '''Leverage POST requests to ADD SEVERAL authors to the backend (database)'''
    results = []
    input = request.json
    author_collection.insert_many(input)
    results.append({"insert_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response


@app.route('/api/book', methods=['DELETE'])
def api_delete_book():
    '''Leverage DELETE requests to REMOVE the first book of the filtered results from the database'''
    results = []
    if 'book_url' in request.args:
        name = request.args['book_url']
        origin_document = book_collection.delete_one({"book_url": name})
    elif 'title' in request.args:
        name = request.args['title']
        print(name)
        origin_document = book_collection.delete_one({"title": name})
    elif 'book_id' in request.args:
        name = request.args['book_id']
        origin_document = book_collection.delete_one({"book_id": name})
    elif 'isbn' in request.args:
        name = request.args['isbn']
        origin_document = book_collection.delete_one({"isbn": name})
    elif 'author_url' in request.args:
        name = request.args['author_url']
        origin_document = book_collection.delete_one({"author_url": name})
    elif 'author' in request.args:
        name = request.args['author']
        origin_document = book_collection.delete_one({"author": name})
    elif 'rating' in request.args:
        name = request.args['rating']
        origin_document = book_collection.delete_one({"rating": name})
    elif 'rating_count' in request.args:
        name = request.args['rating_count']
        origin_document = book_collection.delete_one({"rating_count": name})
    elif 'review_count' in request.args:
        name = request.args['review_count']
        origin_document = book_collection.delete_one({"review_count": name})
    elif 'image_url' in request.args:
        name = request.args['image_url']
        origin_document = book_collection.delete_one({"image_url": name})
    elif 'similar_books' in request.args:
        name = request.args['similar_books']
        origin_document = book_collection.delete_one({"similar_books": name})
    else:
        response = jsonify(results)
        response.status_code = 400  # bad data
        print(response.status_code)
        return "Error"

    print(origin_document.deleted_count)
    print(origin_document)
    results.append({"delete_book_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response

@app.route('/api/author', methods=['DELETE'])
def api_delete_author():
    '''Leverage DELETE requests to REMOVE the first author of the filtered results content from the database'''
    results = []
    if 'name' in request.args:
        name = request.args['name']
        origin_document = author_collection.delete_one({"name": name})
    elif 'author_url' in request.args:
        name = request.args['author_url']
        origin_document = author_collection.delete_one({"author_url": name})
    elif 'author_id' in request.args:
        name = request.args['author_id']
        origin_document = author_collection.delete_one({"author_id": name})
    elif 'rating' in request.args:
        name = request.args['rating']
        origin_document = author_collection.delete_one({"rating": name})
    elif 'rating_count' in request.args:
        name = request.args['rating_count']
        origin_document = author_collection.delete_one({"rating_count": name})
    elif 'review_count' in request.args:
        name = request.args['review_count']
        origin_document = author_collection.delete_one({"review_count": name})
    elif 'image_url' in request.args:
        name = request.args['image_url']
        origin_document =author_collection.delete_one({"image_url": name})
    elif 'related_authors' in request.args:
        name = request.args['related_authors']
        origin_document = author_collection.delete_one({"related_authors": name})
    elif 'author_books' in request.args:
        name = request.args['author_books']
        origin_document = author_collection.delete_one({"author_books": name})
    else:
        response = jsonify(results)
        response.status_code = 400  # bad data
        print(response.status_code)
        return "Error"

    results.append({"delete_author_status": "success"})
    response = jsonify(results)

    response.status_code = 200
    print(response.status_code)
    return response


#CRUD for /book/<name> : the book of the book_id
@app.route("/book", methods=['GET'])
def get_book_render():
    results = []
    input = request.json
    cur = book_collection.find(input)
    for doc in cur:
        results.append(doc)
    return render_template('template.html', result=results)

#CRUD for /book/<name> : the book of the book_id
@app.route("/book", methods=['PUT'])
def put_book_render():
    results = []
    input = request.json
    print(input[0])
    print(input[1])
    cur = book_collection.find_one_and_update(input[0], {"$set": input[1]})
    if cur == None:
        results.append({"put failed": "cannot find the book_id"})
    else:
        for doc in cur:
            results.append(doc)
    return render_template('template.html', result=results)



#CRUD for /book/<name> : the book of the book_id
@app.route("/book", methods=['POST'])
def post_book_render():
    results = []
    input = request.json
    book_collection.insert_one(input)
    results.append({"insert_status": "success"})
    return render_template('template.html', result=results)

#CRUD for /book/<name> : the book of the book_id
@app.route("/book", methods=['DELETE'])
def delete_book_render():
    results = []
    input = request.json
    doc = book_collection.delete_one(input)
    if(doc.deleted_count > 0):
        results.append({"delete_status": "success"})
    else:
        results.append({"delete_status": "no file to be deleted"})
    return render_template('template.html', result=results)



#CRUD for /author/<name> : the author of the author_id
@app.route("/author", methods=['GET'])
def get_author_render():
    results = []
    input = request.json
    cur = book_collection.find(input)
    for doc in cur:
        results.append(doc)
    return render_template('template.html', result=results)

#CRUD for /author/<name> : the author of the author_id
@app.route("/author", methods=['PUT'])
def put_author_render():
    results = []
    input = request.json
    print(input[0])
    print(input[1])
    cur = author_collection.find_one_and_update(input[0], {"$set": input[1]})
    if cur == None:
        results.append({"put failed": "cannot find the author_id"})
    else:
        for doc in cur:
            results.append(doc)
    return render_template('template.html', result=results)

#CRUD for /author/<name> : the author of the author_id
@app.route("/author", methods=['POST'])
def post_author_render():
    results = []
    input = request.json
    author_collection.insert_one(input)
    results.append({"insert_status": "success"})
    return render_template('template.html', result=results)

#CRUD for /author/<name> : the author of the author_id
@app.route("/author", methods=['DELETE'])
def delete_author_render():
    results = []
    input = request.json
    doc = author_collection.delete_one(input)
    if(doc.deleted_count > 0):
        results.append({"delete_status": "success"})
    else:
        results.append({"delete_status": "no file to be deleted"})
    return render_template('template.html', result=results)


#GET /books: a list of all the books in your database
@app.route("/books")
def get_books_render():
    results = []
    cursor = book_collection.find({})
    for doc in cursor:
        results.append(doc)
    return render_template('template.html', result=results)




#GET /authors: a list of authors in your database
@app.route("/authors")
def get_authors_render():
    results = []
    cursor = author_collection.find({})
    for doc in cursor:
        results.append(doc)
    return render_template('template.html', result=results)

#Query 1: Find the author that has the most books stored in your database.
@app.route("/query/most-book-authors")
def get_most_book_author():
    cursor = book_collection.aggregate([
        {"$group": {
            "_id": "$author"
        }},
        {"$sort": {"_id": -1}},
        {"$limit":1}
    ])
    response = jsonify(json_util.dumps(cursor))
    return response


#/////todo/////query 2


#/vis/rank-authors.
@app.route("/vis/rank-authors/")
def chart():
    '''plot graphs to visualize data of authors with high ranks'''
    results=[]
    cursor = author_collection.find().sort("rating", -1)

    i=0
    for x in cursor:
        results.append([x['name'], x['rating']])
        i += 1
        if i > 15:
            break
    legend = "top ranks of authors"
    labels=[]
    values = []

    for item in results:
        values.append(float(item[1]))
        labels.append(item[0])
    print(values)
    print(labels)

    plots = []
    plots.append(make_plot(labels, values))

    return render_template('dashboard.html', plots=plots)

def make_plot(x,y):
    plot = figure(plot_height=300, sizing_mode='scale_width')
    plot.line(x, y, line_width=4)

    script, div = components(plot)
    return script, div


#production server
serve(app, host="0.0.0.0", port=8080)


##deployment server
#app.run()



