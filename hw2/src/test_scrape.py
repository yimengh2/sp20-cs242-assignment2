import unittest
from unittest import TestCase
import requests
from bs4 import BeautifulSoup
from src.scrape import Scrape
import json
from pymongo import MongoClient


class TestScrape(TestCase):

    def test_initial_data(self):
        Scrape.__init__(self)
        print(len(self.data))
        assert len(self.data) == 2

    def test_initial_data_book(self):
        Scrape.__init__(self)
        print(len(self.data['Book']))
        assert len(self.data['Book']) == 0

    def test_initial_data_author(self):
        Scrape.__init__(self)
        print(len(self.data['Author']))
        assert len(self.data['Author']) == 0

    def test_database(self):
        book_page = "https://www.goodreads.com/book/show/3735293-" \
                    "clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
        Scrape().__init__()
        assert (MongoClient("mongodb+srv://yimengh2:AlinaHan910@cluster0"
                            "-oevn3.mongodb.net/test?retryWrites=true&w=m"
                            "ajority").list_database_names())[0] == 'my_database'

    def test_add_one_book_one_author(self):
        book_page = "https://www.goodreads.com/book/show/3735293-" \
                    "clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
        Scrape().main(book_page, 1, 1)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        # print(len(data['Book']))
        assert len(data['Book']) == 1
        assert len(data['Author']) == 1

    def test_add_one_book(self):
        book_page = "https://www.goodreads.com/book/show/3735293-" \
                    "clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
        Scrape().main(book_page, 1, 0)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        # print(len(data['Book']))
        assert len(data['Book']) == 1
        assert len(data['Author']) == 0

    def test_add_one_author(self):
        book_page = "https://www.goodreads.com/book/show/3735293-" \
                    "clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
        Scrape().main(book_page, 1, 1)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        # print(len(data['Book']))
        assert len(data['Book']) == 1
        # print(len(data['Author']))
        assert len(data['Author']) == 1

    def test_different_page_url(self):
        book_page = "https://www.goodreads.com/book/show/85009.Design_Patterns"
        Scrape().main(book_page, 1, 1)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        # print(len(data['Book']))
        assert len(data['Book']) == 1
        # print(len(data['Author']))
        assert len(data['Author']) == 1

    def test_add_multiple_books_and_authors(self):
        book_page = "https://www.goodreads.com/book/show/85009.Design_Patterns"
        Scrape().main(book_page, 10, 5)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        print(len(data['Book']))
        print(len(data['Author']))
        assert len(data['Book']) == 10
        assert len(data['Author']) == 5

    def test_add_hundreds_books_and_authors(self):
        book_page = "https://www.goodreads.com/book/show/85009.Design_Patterns"
        Scrape().main(book_page, 100, 50)
        with open('dataInfo.json') as json_file:
            data = json.load(json_file)
        print(len(data['Book']))
        print(len(data['Author']))
        assert len(data['Book']) == 100
        assert len(data['Author']) == 50


if __name__ == '__main__':
    unittest.main()
