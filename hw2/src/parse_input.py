import argparse
from scrape import Scrape

# start scrape with terminal command
ap = argparse.ArgumentParser(description="Add source page you want to start scraping")
ap.add_argument("--url", required=True, help="url of the page")
ap.add_argument("-num_book", required=True, help="number of book you want to scrape")
ap.add_argument("-num_author", required=True, help="number of author you want to scrape")

args = vars(ap.parse_args())
# display a friendly message to the user
print("You have successfully start scraping {}".format(args["url"]))

book_page = args["url"]
num_book = int(args["num_book"])
num_author = int(args["num_author"])
if num_book > 2000 or num_author > 2000:
    print("Warning! You cannot scrape too many books or too many authors")
else:
    Scrape().main(book_page, num_book, num_author)

# python parse_input.py --url https://www.goodreads.com/book/show/85009.Design_Patterns -num_book 1 -num_author 1
