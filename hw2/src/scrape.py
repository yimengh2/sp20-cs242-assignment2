import os
import json
import argparse
from bs4 import BeautifulSoup
import requests

from pymongo import MongoClient


class Scrape:


    def __init__(self):
        """
        initiate all variables
        """
        #mongodb website to check status
        #https://cloud.mongodb.com/v2/5e5b77a18d40110c99891a73#me
        # trics/host/ace76154e4211f22c73704aa74dfbe85/status/fixed-opcounters-chart
        self.client = MongoClient("mongodb+srv://yimengh2:AlinaHan910"
                                  "@cluster0-oevn3.mongodb.net/test?retryWrites=true&w=majority")
        # get a database
        self.db = self.client.my_database

        self.data = {'Book': [], 'Author': []}
        # limit total number of books
        self.book_count = 0
        # limit total number of authors
        self.author_count = 0

        # author_page = "https://www.goodreads.com/author/show/45372.Robert_C_Martin"
        self.list_author_url = []
        self.list_book_url = []

    def add_book_info(self, quote_page, num_book, num_author):
        """given quote page url, get info and append to list data in Json type
        """
        # print(r.text)
        soup = BeautifulSoup(requests.get(quote_page).text, 'html.parser')

        # print(soup.prettify())
        # print(soup.title)
        title = soup.find('h1', {'id': 'bookTitle'}).text
        author = soup.find("span", itemprop="name").text
        # print(bookTitle)
        # print(bookAuthor)
        book_url = quote_page
        self.list_book_url.append(book_url)
        book_id = soup.find('input', {'id': 'book_id'}).get('value')

        isbn = soup.find("div", {"class": "infoBoxRowTitle"}).text

        author_url = soup.find("meta", property="books:author")["content"]
        rating = soup.find("span", itemprop="ratingValue").text
        rating_count = soup.find("meta", itemprop="ratingCount")["content"]

        review_count = soup.find("meta", itemprop="reviewCount")["content"]

        # images = soup.find_all('img', class_="bookCoverPrimary")
        image_url = soup.find('div', {"class": "bookCoverPrimary"}).img['src']
        # print(image_url)

        similar_books = ""
        product_divs = soup.findAll('div', attrs={'class': 'listImgs'})
        for div in product_divs:
            similar_books = div.a['href']
            break
        if self.book_count < num_book:
            self.book_count = self.book_count + 1
            self.data['Book'].append({
                "book_url": book_url,
                "title": title.strip(),
                "book_id": book_id,
                "isbn": isbn,
                "author_url": author_url,
                "author": author,
                "rating": rating.strip(),
                "rating_count": rating_count,
                "review_count": review_count,
                "image_url": image_url,
                "similar_books": "https://www.goodreads.com" + similar_books
            })

            # insert while scraping
            self.db.book_collection.insert_one({
                "book_url": book_url,
                "title": title.strip(),
                "book_id": book_id,
                "isbn": isbn,
                "author_url": author_url,
                "author": author,
                "rating": rating.strip(),
                "rating_count": rating_count,
                "review_count": review_count,
                "image_url": image_url,
                "similar_books": "https://www.goodreads.com" + similar_books
            })

        '''
        if author page has not been scraped, scape the author page
        '''
        if (author_url not in self.list_author_url) and (self.author_count < num_author):
            #self.author_count = self.author_count + 1
            self.add_author_info(author_url, num_book, num_author)

    def add_author_info(self, quote_page, num_book, num_author):
        """
        given quote page url, get author info and append to list data in Json type
        :param quote_page:

        """
        # print(r.text)
        soup = BeautifulSoup(requests.get(quote_page).text, 'html.parser')

        name = soup.find("span", itemprop="name").text

        author_url = quote_page

        self.list_author_url.append(author_url)

        author_id = soup.find('input', {'id': 'unique_id'}).get('value')

        rating = soup.find("span", itemprop="ratingValue").text

        rating_count = soup.find("span", itemprop="ratingCount").text

        review_count = soup.find("span", itemprop="reviewCount").text

        image_url = soup.find('div', {"class": "leftContainer authorLeftContainer"}).img['src']

        related_authors = ""
        author_books = ""
        links = soup.find_all('a', href=True)
        for i in links:
            if "similar" in i['href']:
                related_authors = i['href']
                break
            if "list" in i['href']:
                author_books = i['href']
        if self.author_count <= num_author:
            self.author_count += 1
            self.data['Author'].append({
                "name": name,
                "author_url": author_url,
                "author_id": author_id,
                "rating": rating,
                "rating_count": rating_count.strip(),
                "review_count": review_count.strip(),
                "image_url": image_url,
                "related_authors": "https://www.goodreads.com" + related_authors,
                "author_books": "https://www.goodreads.com" + author_books
            })

            # insert while scraping
            self.db.author_collection.insert_one({
                "name": name,
                "author_url": author_url,
                "author_id": author_id,
                "rating": rating,
                "rating_count": rating_count.strip(),
                "review_count": review_count.strip(),
                "image_url": image_url,
                "related_authors": "https://www.goodreads.com" + related_authors,
                "author_books": "https://www.goodreads.com" + author_books
            })

        # get more books from same author
        for link in soup.findAll('a', {'class': 'bookTitle'}):
            if self.book_count > num_book:
                break
            if (("https://www.goodreads.com" + link['href']) not in self.list_book_url) \
                    and ("book" in link['href']):
                try:
                    # print("https://www.goodreads.com" + link['href'])
                    #self.book_count = self.book_count + 1
                    self.add_book_info("https://www.goodreads.com"
                                       + link['href'], num_book, num_author)
                except KeyError:
                    pass

    def get_book_from_list(self, index, num_book, num_author):
        """
        get list of books from the first book source
        :param index:
        """
        # global book_count
        quote_page = self.data['Book'][index].get("similar_books")
        # print('quote_page:')
        # print(quote_page)
        soup = BeautifulSoup(requests.get(quote_page).text, 'html.parser')

        for link in soup.findAll('a', {'class': 'bookTitle'}):
            # print("link for similar books: ")
            # print("https://www.goodreads.com" + link['href'])
            if ("https://www.goodreads.com" + link['href']) not in self.list_book_url:
                try:
                    self.book_count = self.book_count + 1
                    # print(book_count)
                    self.add_book_info("https://www.goodreads.com" +
                                       link['href'], num_book, num_author)

                except KeyError:
                    pass

    def start(self, book_page, num_book, num_author):
        """
        insert book_page, start scraping
        """

        self.add_book_info(book_page, num_book, num_author)
        self.get_book_from_list(0, num_book, num_author)

    def handle_end(self):
        """
        after scraping, write all data into json file
        add all data in json file to database
        """
        # empty json file before dump

        json_file = open("dataInfo.json", mode="r")
        json.load(json_file)
        with open('dataInfo.json', 'w') as outfile:
            json.dump(self.data, outfile)

            # with open('dataInfo.json') as f:
            #     file_dadta = json.load(f)

        # Import your JSON into the database
        if len(self.data['Book']) != 0:  # cannot insert empty list
            self.db.book_collection.insert_many(self.data['Book'])
        if len(self.data['Author']) != 0:
            self.db.author_collection.insert_many(self.data['Author'])

    # add_author_info(author_page)
    # print(list_book_url)

    # print(book_count)
    # print(author_count)

    # collection_id = collection.insert_one(json.encode(data, cls=JSONEncoder)).inserted_id

    def main(self, book_page, num_book, num_author):
        """
        main function, start scraping and handle data when scraping ends
        """
        self.start(book_page, num_book, num_author)
        self.handle_end()


# comment when testing
if __name__ == "__main__":
    book_page = "https://www.goodreads.com/book/show/3735293-" \
                "clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
    Scrape().main(book_page, 200, 50)

    # PRINT ENV_KEY
    print(os.environ['ENV_KEY'])

