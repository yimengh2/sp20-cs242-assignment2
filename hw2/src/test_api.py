import requests as req
import unittest
from unittest import TestCase

class TestApi(TestCase):
    def test_status_get_book_by_title(self):
        res = req.get('http://0.0.0.0:8080/api/books?title=Coder')
        assert res.status_code == 200

    def test_get_book_by_title(self):
        res = req.get('http://0.0.0.0:8080/api/books?title=Coder')
        data = res.json()
        assert "The Clean Coder: A Code of Conduct for Professional Programmers" in data[0]

    def test_and_get_book_by_title(self):
        res = req.get('http://0.0.0.0:8080/api/books?title=CoderAND_ssss')
        data = res.json()
        assert len(data)==0

    def test_or_get_book_by_title(self):
        res = req.get('http://0.0.0.0:8080/api/books?title=CoderOR_for')
        data = res.json()
        assert "The Clean Coder: A Code of Conduct for Professional Programmers" in data[0]

    def test_put_books_with_new_attribute(self):
        res = req.put(url = "http://0.0.0.0:8080/api/books",
                      params ={"book_url": "https://www.goodreads.com/book/show/3735293-clean-code"},
                      json = {"wrong_attri": "aa"})
        data = res.json()
        assert "wrong_attri" in data[0]

    def test_put_books_with_updated_attribute(self):
        res = req.put(url = "http://0.0.0.0:8080/api/books",
                      params ={"book_url": "https://www.goodreads.com/book/show/3735293-clean-code"},
                      json = {"book_id": "test_book_id"})
        data = res.json()
        assert "test_book_id" in data[0]

    def test_post_books_status(self):
        res=req.post(url = "http://0.0.0:8080/api/book", json = {"test_post_book": "test_test"})
        data = res.json()
        assert "insert_status" in data[0]

    def test_post_books(self):
        res=req.post(url = "http://0.0.0:8080/api/book", json = {"title": "test_post_title"})
        get_res = req.get('http://0.0.0.0:8080/api/books?title=test_post_title')
        data = get_res.json()
        assert "test_post_title" in data[0]

    def test_delete_books(self):
        res = req.post(url="http://0.0.0:8080/api/book", json={"title": "test_post_title"})
        delete_res = req.delete(url="http://0.0.0:8080/api/book?title=test_post_title")
        data = delete_res.json()
        assert {'delete_book_status': 'success'} == data[0]

    def test_delete_authors(self):
        res = req.post(url="http://0.0.0:8080/api/author", json={"name": "test"})
        delete_res = req.delete(url="http://0.0.0:8080/api/author?name=test")
        data = delete_res.json()
        print(data)
        assert {'delete_author_status': 'success'} == data[0]

if __name__ == '__main__':
    unittest.main()